function calculateGrade() {
    const weights = [0.6, 0.2, 0.1, 0.1];
    const maxScores = [10, 10, 5, 5];
    let totalScore = 0;

    // Remove all highlights before calculation
    const allCells = document.querySelectorAll(`#grading-rubric td`);
    allCells.forEach(cell => {
        cell.className = ""; // Reset all classes
    });

    for(let i = 0; i < 4; i++){
        const scoreElement = document.getElementById(`score${i+1}`);
        if(!scoreElement.value){
            document.getElementById("result").innerHTML = "Please select all values";
            return;
        }
        const score = parseFloat(scoreElement.value);
        totalScore += (score / maxScores[i]) * weights[i] * 100;

        highlightScore(i, score, maxScores[i]);  // Call the function to highlight the score in the table
    }

    document.getElementById("result").innerHTML = `Grade: ${totalScore.toFixed(0)}%`;
}

function highlightScore(index, score) {
    const rubricRows = document.querySelectorAll("#grading-rubric tbody tr");
    const rubricRow = rubricRows[index];
    const cells = rubricRow.querySelectorAll("td");

    // Define score ranges for each row
    let scoreRanges;
    if (index === 0 || index === 1) {  // For the Functionality and Usability rows
        scoreRanges = ['0-2', '3-4', '5-8', '9-10'];
    } else {  // For the Documentation and Code Quality rows
        scoreRanges = ['0-1', '2', '3-4', '5'];
    }

    // Identify the score range that the score falls into
    let scoreRange;
    for (let range of scoreRanges) {
        const [min, max] = range.split('-').map(Number);  // Split the range into min and max values
        if (!max) {  // If max is undefined, it means that the range is a single number
            if (score === min) {
                scoreRange = range;
                break;
            }
        } else {  // If max is defined, it means that the range is a range of numbers
            if (score >= min && score <= max) {
                scoreRange = range;
                break;
            }
        }
    }

    // Identify the cell to be highlighted based on the score range
    for (let i = 1; i < cells.length; i++) {
        const cell = cells[i];
        const cellText = cell.innerText;
        if (cellText.includes(scoreRange)) {
            cell.classList.add("table-active");
            break;
        }
    }
}

document.addEventListener("DOMContentLoaded", function() {
    populateScores([10, 10, 5, 5], ['score1', 'score2', 'score3', 'score4']);
});

function populateScores(maxScores, ids){
    maxScores.forEach((maxScore, index) => {
        let select = document.getElementById(ids[index]);
        for(let i = 0; i <= maxScore; i++){
            let option = document.createElement("option");
            option.text = i;
            option.value = i;
            select.appendChild(option);
        }

        select.addEventListener('change', () => {  // Add the event listener
            calculateGrade();
        });
    });
}

document.getElementById('copyTable').addEventListener('click', function() {
    var gradingRubric = document.getElementById('grading-rubric');
    html2canvas(gradingRubric).then(function(canvas) {
        var context = canvas.getContext('2d');
        context.globalCompositeOperation = 'destination-over';
        context.fillStyle = '#FFFFFF';
        context.fillRect(0, 0, canvas.width, canvas.height);
        var img = canvas.toDataURL("image/png");
        var link = document.createElement('a');
        link.download = 'rubric.png';
        link.href = img;
        link.click();
    });
});
